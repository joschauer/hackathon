package de.nhka15.pokefile.gamestate;

import android.graphics.Canvas;
import android.view.MotionEvent;

public abstract class Gamestate {
    
    public static Fight fightGamestate = new Fight();
    public static Menu  menuGamestate  = new Menu();
    
    public abstract void update(long elapsedMillis);
    
    public abstract void render(Canvas c);

    public abstract void touchEvent(MotionEvent event);
}