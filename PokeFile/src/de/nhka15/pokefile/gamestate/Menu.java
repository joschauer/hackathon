package de.nhka15.pokefile.gamestate;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

public class Menu extends Gamestate {

    long calls = 0;
    
    @Override
    public void update(long elapsedMillis) {
        calls++;
    }

    @Override
    public void render(Canvas c) {
        c.drawText(String.valueOf(calls), c.getWidth()/2, c.getHeight()/2, new Paint());
    }

    @Override
    public void touchEvent(MotionEvent event) {
	// TODO Auto-generated method stub
	
    }
}
