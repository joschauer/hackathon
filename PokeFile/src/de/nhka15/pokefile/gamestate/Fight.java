package de.nhka15.pokefile.gamestate;

import android.R.color;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.TranslateAnimation;
import de.nhka15.pokefile.PokeFile;
import de.nhka15.pokefile.attack.Attack;
import de.nhka15.pokefile.drawable.Menu;
import de.nhka15.pokefile.drawable.MenuEntry;
import de.nhka15.pokefile.monster.Monster;
import de.nhka15.pokefile.monster.TxtMonster;

final class Fight extends Gamestate {

    Monster myMonster, enemy;
    Menu m;
    int curAction;
    boolean attack = false;
    
    public static final int ACTION_START 	= 0x00;
    public static final int ACTION_FIGHT 	= 0x01;
    public static final int ACTION_END 		= 0x02;

    private Paint p1, p2;
    
    public Fight() {
        myMonster = new TxtMonster(Monster.Direction.BACK);
        enemy = new TxtMonster(Monster.Direction.FRONT);
        changeAction(ACTION_START);
        m = new Menu();
        p1 = new Paint();
        p2 = new Paint();
        p1.setColor(0xffffffff);
        p2.setColor(0xff000000);
        m.setEntries(myMonster.getAttacks());
    }
    
    @Override
    public void render(Canvas c) {
	    // Ich bin zu faul das richtig zu machen, so passts auch
	    c.drawRect(new Rect(0, 0, PokeFile.WIDTH, PokeFile.HEIGHT / 6), p1);
	    c.drawRect(new Rect(0,PokeFile.HEIGHT/6,PokeFile.WIDTH, PokeFile.HEIGHT), p2);
        enemy.draw(c);
        myMonster.draw(c);
        m.draw(c);
    }

    @Override
    public void update(long elapsedMillis) {
        enemy.update(elapsedMillis);
        myMonster.update(elapsedMillis);
        
        switch (curAction) {
        case ACTION_START:
            
            break;
        }
    }
    
   private void changeAction(int newAction) {
	curAction = newAction;
	switch (curAction) {
	case ACTION_START:
	    enemy.setPosition(PokeFile.WIDTH + enemy.getWidth(), 200);
	    enemy.moveTo(PokeFile.WIDTH*3 / 4, 200);
	    
	    myMonster.setPosition(-myMonster.getWidth(), PokeFile.HEIGHT - 200);
	    myMonster.moveTo(PokeFile.WIDTH/4, PokeFile.HEIGHT - 200);
	    break;
	}
    }

    @Override
    public void touchEvent(MotionEvent event) {
        Log.d("", event.toString());
        MenuEntry entry = null;
        if(event.getAction() == MotionEvent.ACTION_UP) {
           entry =  m.getItemAt((int) event.getX(), (int) event.getY());
        }
        if(entry != null && entry instanceof Attack) {
            attack = true;
            TranslateAnimation ac = null;
        }
    }

}
