package de.nhka15.pokefile;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class PokeFile extends Activity {
    
    public static int WIDTH;
    public static int HEIGHT;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
        
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        WIDTH = point.x;
        HEIGHT = point.y;
        
        setContentView(R.layout.activity_poke_file);
    }
    
    @Override
    protected void onResume() {
        
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
	
        super.onResume();
    }
}
