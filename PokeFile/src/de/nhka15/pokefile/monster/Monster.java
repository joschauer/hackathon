package de.nhka15.pokefile.monster;

import de.nhka15.pokefile.attack.Attack;
import de.nhka15.pokefile.drawable.GameObject;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;

import de.nhka15.pokefile.PokeView;
import de.nhka15.pokefile.R;

public class Monster extends GameObject{
    
    protected Attack[] attacks = { Attack.backup, Attack.grammar};
    
    public enum MonsterType {
	Text, Picture, Archive
    }
    
    public enum Direction {
	FRONT, BACK
    }
    
    protected int hp;
    protected int lvl;
    protected MonsterType type;

    protected BitmapDrawable shape;
    protected ShapeDrawable ground;
    
    protected Direction curDir;
	
    public Monster(Direction d) {
        hp = 0;
        lvl = 1;
        curDir = d;
        width = 64*5;
        height = 64*5;
        ground = new ShapeDrawable(new OvalShape());
        ground.getPaint().setColor(0xff909090);
        pos = new Point(-600,-600);
    }
    
    public void draw(Canvas c) {
        if (shape == null) {
            Bitmap map;
            if (curDir == Direction.FRONT) {
                map = BitmapFactory.decodeResource(PokeView.res, R.drawable.txt_front);
            } else
                map = BitmapFactory.decodeResource(PokeView.res, R.drawable.txt_back);
            shape = new BitmapDrawable(PokeView.res, map);
            shape.getPaint().setColor(0xff000000);
        }

        ground.draw(c);
        shape.draw(c);
    }
    
    public void update(long elapsedMillis) {
	super.update(elapsedMillis);
	if(shape != null)
	    shape.setBounds(pos.x - width/2, pos.y - width/2, pos.x + (width/2), pos.y + (height/2));
	    ground.setBounds(pos.x - width, pos.y - (height/3) + 100, pos.x + width, pos.y + height/3 + 100);
    }
    
    public Attack[] getAttacks() {
	return attacks;
    }
}

