package de.nhka15.pokefile.monster;

import de.nhka15.pokefile.attack.Attack;

public class TxtMonster extends Monster {
    public TxtMonster(Direction d) {
	super(d);
	type = Monster.MonsterType.Text;
	attacks = new Attack[4];
	attacks[0] = Attack.grammar;
	attacks[1] = Attack.encode;
	attacks[2] = Attack.delete;
	attacks[3] = Attack.empty;
    }
    
    
}
