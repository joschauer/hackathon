package de.nhka15.pokefile;

import de.nhka15.pokefile.attack.Attack;

public class Type {
	Attack[] standartAttacks = new Attack[4];
	DataType dataType; 
	
	public enum DataType{
		txt, doc, docx,  gif, jpg, png,  zip, rar,  neutral
	}
	
	public Type(DataType dataType){
		this.dataType = dataType;
		//setStandartAttacks(dataType);
	}
	
}
