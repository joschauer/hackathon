package de.nhka15.pokefile.attack;

import de.nhka15.pokefile.drawable.MenuEntry;

public abstract class Attack extends MenuEntry {
    protected int strength;
    
    public static Encode encode = new Encode();
    public static Backup backup  = new Backup();
    public static Grammar grammar = new Grammar();
    public static Delete delete = new Delete();
    public static EmptyAttack empty = new EmptyAttack();
    
    public Attack() {
        super();
    }

    @Override
    public String toString() {
	return text;
    }
}
