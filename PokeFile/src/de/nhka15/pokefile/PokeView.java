package de.nhka15.pokefile;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import de.nhka15.pokefile.gamestate.Gamestate;

public class PokeView extends SurfaceView implements SurfaceHolder.Callback {
    
    public static Resources res;
    
    private boolean running;
    
    final SurfaceHolder holder;
    Thread game;
    
    public static int frames;
    public static long lastFrameUpdate;
    
    Gamestate curGamestate;
    
    private class GameLoop implements Runnable {
        
        @Override
        public void run() {
            running = true;
            long lastUpdate = System.currentTimeMillis();
            frames = 0;
            lastFrameUpdate = System.currentTimeMillis();
            String fps = "";
            String ups = "";
            long updates= 0;
            double targetMillis = 1000.0/60;
            double neededUpdates = 1;

            while(running) {
                if (curGamestate != null) {
                    synchronized (holder) {
                        Canvas c = holder.lockCanvas();
                        if (c != null) {
                            c.drawColor(Color.WHITE);
                            neededUpdates += (System.currentTimeMillis() - lastUpdate)/targetMillis;
                            lastUpdate = System.currentTimeMillis();
                            while(neededUpdates >= 1) {
                                curGamestate.update(0);
                                neededUpdates--;
                                updates++;
                            }

                            curGamestate.render(c);
                            frames++;
                            
                            double delta = (System.currentTimeMillis() - lastFrameUpdate)/1000.0;
                            if(delta >= 0.5) {
                        	    fps = String.valueOf(Math.round(frames/delta));
                                ups = String.valueOf(Math.round(updates/delta));
                        	    frames = 0;
                                updates = 0;
                        	    lastFrameUpdate = System.currentTimeMillis();
                            }
                            c.drawText(fps + "fps  " + ups + "tps", 20, 20, new Paint());
                            
                            holder.unlockCanvasAndPost(c);
                        }
                    } 
                }
            }
        }
        
    }
    
    public PokeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        holder = getHolder();
        holder.addCallback(this);
        game = new Thread(new GameLoop());
        game.setName("GameLoop");
        game.setPriority(Thread.MAX_PRIORITY);
        curGamestate = Gamestate.fightGamestate;
        res = getResources();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        game.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        running = false;
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
	curGamestate.touchEvent(event);
        return super.onTouchEvent(event);
    }
}
