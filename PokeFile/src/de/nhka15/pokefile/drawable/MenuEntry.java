package de.nhka15.pokefile.drawable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.PaintDrawable;

public class MenuEntry extends Button {
    
    public MenuEntry() {
	this("");
    }
    
    public MenuEntry(String text) {
    super(text);
	this.text = text;
    }
    
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }
}
