package de.nhka15.pokefile.drawable;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;

public abstract class GameObject {
    
    protected int width;
    protected int height;
    protected int speed;
    
    protected Point pos;
    protected Point dest;
    protected boolean moving;
    protected boolean movable;

	protected Rect bounds;
    
    public void update(long elapsedMillis) {
	if(moving) {
	    if(pos.x/speed < dest.x/speed)
		pos.x+= speed;
	    else if(pos.x/speed > dest.x/speed)
		pos.x-= speed;
	    
	    if(pos.y/speed < dest.y/speed)
		pos.y+= speed;
	    else if(pos.y/speed > dest.y/speed)
		pos.y-= speed;
	    
	    if(pos.x/speed == dest.x/speed && pos.y/speed == dest.y/speed) {
		moving = false;
	    }
	}
    }
    
    public abstract void draw(Canvas c);
    
    public void setPosition(int x, int y) {
   	setPosition(x, y, 3);
    }
       
    public void setPosition(int x, int y, int speed) {
   	pos.x = x - width/2;
   	pos.y = y - height/2;
   	this.speed = speed;
    }

	public void moveToRelative(int rx, int ry) {
		moveTo(pos.x + +rx, pos.y + ry);
	}
       
    public void moveTo(int x, int y) {
   	dest = new Point(x, y);
   	moving = true;
    }

	public void setBounds(Rect rect) {
		this.bounds = rect;
	}

	public Rect getBounds() {
		return bounds;
	}
       
    public int getWidth() {
   	return width;
    }
       
    public int getHeight() {
   	return height;
    }
}
