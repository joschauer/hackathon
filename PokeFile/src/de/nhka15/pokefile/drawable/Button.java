package de.nhka15.pokefile.drawable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.PaintDrawable;

/**
 * Created by Joshua on 06.12.2015.
 */
public class Button extends GameObject {

    PaintDrawable background;
    protected Paint p;
    public String text;

    public Button(String text) {
        super();
        background = new PaintDrawable(0xffffffff);
        background.setCornerRadius(30);
        p = new Paint();
        p.setStrokeWidth(50);
        p.setTextSize(50);
    }

    @Override
    public void draw(Canvas canvas) {
        background.draw(canvas);
        if(text != null) {
            canvas.drawText(text, bounds.left + 50, bounds.top + 60, p);
        }
    }

    @Override
    public void setBounds(Rect rect) {
        super.setBounds(rect);
        background.setBounds(rect);
    }
}
