package de.nhka15.pokefile.drawable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.PaintDrawable;
import de.nhka15.pokefile.PokeFile;

public class Menu extends GameObject{
    
    PaintDrawable border;
    MenuEntry[] entries;
    
    public Menu() {
		super();
		border = new PaintDrawable(0xffB0B0B0);
		bounds = new Rect(PokeFile.WIDTH*5/10, PokeFile.HEIGHT*3/4, PokeFile.WIDTH, PokeFile.HEIGHT);
		border.setBounds(bounds);
		//border.getPaint().setColor(0xffffffff);
		float[] radii = { 20, 20, 0, 0, 0, 0, 20, 20};
		border.setCornerRadii(radii);
    }
    
    public void setEntries(MenuEntry[] entries) {
		this.entries = entries;
		int l = bounds.left;
		int r = bounds.right;
		int t = bounds.top;
		int b = bounds.bottom;
		int spacer = 10;
		int h = (b - t - 3*spacer)/2;
		int w = (r - l - 4*spacer)/2;
		entries[0].setBounds(new Rect(l + spacer, t + spacer, l + spacer + w, t + spacer + h));
		entries[1].setBounds(new Rect(PokeFile.WIDTH - spacer - w, t + spacer, PokeFile.WIDTH - spacer, t + spacer + h));
		entries[2].setBounds(new Rect(l + spacer, t + h + 2*spacer, l + spacer + w, t + 2*h + 2*spacer));
		entries[3].setBounds(new Rect(PokeFile.WIDTH - spacer - w, t + h + 2*spacer, PokeFile.WIDTH - spacer, t +2*h + 2*spacer));
    }
    
    public void draw(Canvas c) {
		border.draw(c);
		for(MenuEntry m : entries) {
			m.draw(c);
		}
    }
    
    public MenuEntry getItemAt(int x, int y) {
	for(MenuEntry e : entries) {
	    if(e.getBounds().contains(x, y))
		return e;
	}
	return null;
    }
}
